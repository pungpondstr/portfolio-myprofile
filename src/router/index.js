import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About')
  },
  {
    path: '/projects',
    name: 'Projects',
    component: () => import('../views/Projects')
  },
  {
    path: '/contact',
    name: 'Contact',
    component: () => import('../views/Contact')
  }
]

const router = new VueRouter({
  routes
})

export default router
